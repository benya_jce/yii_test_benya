<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "deal".
 *
 * @property integer $id
 * @property integer $leadId
 * @property string $name
 * @property integer $amount
 */
class Deal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['leadId', 'amount'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'leadId' => 'Lead ID',
            'name' => 'Name',
            'amount' => 'Amount',
        ];
    }
	
	public function getOwneLead()
    {
        return $this->hasOne(Lead::className(), ['Id' => 'leadId']);
    }
	
	public static function getLeads()
	{
		$allStatuses = self::find()->all();
		$allStatusesArray = ArrayHelper::
					map($allStatuses, 'id', 'leadId');
		return $allStatusesArray;						
	}
	
	public static function getLeadsWithAllLeads()
	{
		$allLeads = self::getLeads();
		$allLeads[-1] = 'All Leads';
		$allLeads = array_reverse ( $allLeads, true );
		return $allLeads;	
	}
	
	
}
